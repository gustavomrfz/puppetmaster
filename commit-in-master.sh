#!/bin/bash

BRANCH=$(git symbolic-ref HEAD 2>/dev/null | awk  'BEGIN { FS="/"; } { print $3; }')
PUPPET='puppetserver'
git push origin ${BRANCH} \
&& ssh puppetdeploy@${PUPPET} "sudo -S /usr/local/bin/puppet-deploy"
