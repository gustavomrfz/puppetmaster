# == Class: base
#
class base {
  file { '/etc/motd':
    content => "${::fqdn}\nManged by Gustavo. Puppet Version ${::puppetversion}"
  }
}
