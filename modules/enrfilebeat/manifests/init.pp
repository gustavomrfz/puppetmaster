class enrfilebeat {
  class {'filebeat':
    outputs => {
      hosts => [
        'elk:5044'
      ]
    }
  }
  filebeat::prospector {'syslog':
    paths => [
      '/var/log/syslog'
    ]
  }

  filebeat::prospector {'nginx-access':
    paths => [
      '/var/log/nginx/access.log'
    ]
  }
}
