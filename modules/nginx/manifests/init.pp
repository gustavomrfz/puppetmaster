class nginx {

    include nginx::install

    class {'nginx::vhost':
        servername           => 'enredadera.net',
        index_files          => ['index.php', 'index.htm', 'index.html'],
        fastcgi_on           => true,
        ssl                  => true,
        sslcert              => 'server.crt',
        sslkey               => 'server.key',
        locations_file       => 'locations_wordpress.conf',
        error_pages          => 'errorpages_wordpress.conf'
    }
}
