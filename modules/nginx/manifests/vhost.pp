class nginx::vhost (
  $listen                     = 80,
  $listen_https               = 443,
  $servername                 = '',
  $root_path                  = "/var/www/${servername}",
  $index_files                = [],
  $conf_dir                   = '/etc/nginx/conf.d',
  $ssl                        = '',
  $sslcert                    = nil,
  $sslkey                     = nil,
  $fastcgi_on                 = false,
  $locations_file  	      = 'locations_wordpress.conf',
  $error_pages                = 'errorpages_wordpress.conf',
  ){

  if ( $facts['os']['lsbdistid'] == 'Ubuntu' or $facts['os']['lsbdistid'] == 'Debian' ) {
    $fastcgi_php70_file   = 'fastcgi_php7.0_debian.conf'
    file { 'fastcgi_php7.0_debian.conf':
      ensure           => file,
      mode             => '0644',
      source           => 'puppet:///modules/nginx/fastcgi_php7.0_debian.conf',
      path             => '/etc/nginx/conf.d/fastcgi_php7.0_debian.conf',
      require          => Package['nginx-full'],
    }
  } else {
    #TODO
    $fastcgi_php70_file   = 'fastcgi_php7.0_debian.conf'
    file { 'fastcgi_php7.0_debian.conf':
      ensure           => file,
      mode             => '0644',
      source           => 'puppet:///modules/nginx/fastcgi_php7.0_debian.conf',
      path             => "${conf_dir}/fastcgi_php7.0_debian.conf",
      require          => Package['nginx-full'],
    }
  }
  file { "${locations_file}":
    ensure             => file,
    mode               => '0644',
    source             => "puppet:///modules/nginx/${locations_file}",
    path               => "${conf_dir}/${locations_file}",
    require            => Package['nginx-full'],
  }
  file { "${error_pages}":
    ensure             => file,
    mode               => '0644',
    source             => "puppet:///modules/nginx/${error_pages}",
    path               => "${conf_dir}/${error_pages}",
    require            => Package['nginx-full'],
  }

  file { 'default-available':
    ensure             => absent,
    path               => '/etc/nginx/sites-available/default',
  }
  file { 'default-enabled':
    ensure             => absent,
    path               => '/etc/nginx/sites-enabled/default',
  }
  file { "/etc/nginx/sites-available/${servername}":
    ensure             => file,
    mode               => '0644',
    content            => template('nginx/vhost.erb'),
  }
  file { "/etc/nginx/sites-enabled/${servername}":
    ensure             => present,
    mode               => '0644',
    target             => "/etc/nginx/sites-available/${servername}"
  }
  if ( $sslcert != nil ){
    $ssl_cert_path       = "/etc/nginx/ssl/${sslcert}"
    file { "${sslcert}":
      ensure             => file,
      mode               => '0444',
      source             => "puppet:///modules/nginx/ssl/${sslcert}",
      path               => "${ssl_cert_path}",
    }
  }
  if ( $sslkey != nil ){
    $ssl_key_path        = "/etc/nginx/ssl/${sslkey}"
    file { "${sslkey}":
      ensure             => file,
      mode               => '0444',
      source             => "puppet:///modules/nginx/ssl/${sslkey}",
      path               => "${ssl_key_path}",
    }
  }
}
