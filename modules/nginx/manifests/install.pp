class nginx::install {

  package {'nginx-common':
    ensure    => 'installed',
  }

  package {'nginx-full':
    ensure    => 'installed',
    require   => Package['nginx-common'],
  }

  file { 'nginx.conf':
    ensure    => file,
    mode      => '0644',
    source    => 'puppet:///modules/nginx/nginx.conf',
    path      => '/etc/nginx/nginx.conf',
    require   => Package['nginx-full'],
  }

  file { 'ssl':
    ensure    => directory,
    path      => '/etc/nginx/ssl',
    mode      => '0644',
  }
}
