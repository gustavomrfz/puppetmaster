#!/bin/bash

SITE="/etc/puppet/code/manifests/site.pp"
MODULES="/etc/puppet/code/modules"
PUPPET="/usr/bin/puppet"

sudo ${PUPPET} apply ${SITE} --modulepath=${MODULES} $*
