node 'puppetserver' {
  include base
  include puppet
}

node 'filebeat' {
  include enrfilebeat
}

node 'elk' {

  class { 'java':
    distribution => 'jre',
  }

  class { 'logstash':
    jvm_options => [
      '-Xms1g',
      '-Xmx1g',
      ],
    require => Class['java'],
  }

  logstash::patternfile { 'nginx_pattern':
    source => 'puppet:///modules/logstash/nginx.pattern',
  }
  logstash::configfile { 'inputs':
    content => template('logstash/nginxaccess_input.erb'),
  }
  logstash::configfile { 'outputs':
    content => template('logstash/nginxaccess_output.erb'),
  }
  logstash::configfile { 'filters':
    content => template('logstash/nginxaccess_filter.erb'),
  }

  class { 'elasticsearch':
    jvm_options => [
      '-Xms2500m',
      '-Xmx2500m',
    ],
    api_protocol            => 'http',
    api_host                => 'localhost',
    api_port                => 9200,
    api_timeout             => 10,
    require => Class['logstash'],
    instances => {
    'es-01' => {
      'config' => {
        'network.host' => '0.0.0.0'
        }
      }
    },
  }
  apt::source { 'kibana-repo':
  location => 'https://artifacts.elastic.co/packages/5.x/apt',
  release  => 'stable',
  repos    => 'main',
  pin      => '-10',
  key      => {
    'id'     => '46095ACC8548582C1A2699A9D27D666CD88E42B4',
    'server' => 'pgp.mit.edu',
  },
  include  => {
    'src' => false,
    'deb' => true,
  },
}
  class { 'kibana':
    ensure => '5.0',
    config => {
      'server.port' => '5601',
      'server.host' => '0.0.0.0',
    },
  }
  kibana_plugin { 'beats-dashboards':
    url => 'https://download.elastic.co/beats/dashboards/beats-dashboards-1.2.2.zip',
    version => '1.2.2',
    ensure => present,
  }
  kibana_plugin { 'filebeat':}
 	kibana_plugin { 'x-pack':}

}
